# Loan Applications Management

Multi-threaded Loan Applications Management based on Django, and Twilio + Sendgrid

## Installation
To run the app install the required library and dependencies:

`pip install -r loanApp/requirements.txt`

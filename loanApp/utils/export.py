from datetime import datetime
from django.http import HttpResponse
from openpyxl import Workbook
from openpyxl.styles import Font, Alignment
from openpyxl.utils import get_column_letter


def export_to_xlsx(queryset, title):
    """
    Exports the queryset to Excel (XLSX) file.
    """

    response = HttpResponse(
        content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
    )
    response['Content-Disposition'] = 'attachment; filename={date}-{title}.xlsx'.format(
        date=datetime.now().strftime('%Y-%m-%d'), title=title,
    )
    workbook = Workbook()

    # Get active worksheet/tab
    worksheet = workbook.active
    worksheet.title = title

    # Define the titles for columns
    columns = [
        ('Sr. No.', 10),
        ('Staff ID', 10),
        ('Amount', 20),
        ('Date', 20),
        ('Description', 70),
    ]

    row_num = 1
    header_font = Font(bold=True)
    # Assign the titles for each cell of the header
    for col_num, (column_title, column_width) in enumerate(columns, 1):
        cell = worksheet.cell(row=row_num, column=col_num)
        cell.value = column_title
        cell.font = header_font
        cell.alignment = Alignment(horizontal='center')
        # set column width
        column_letter = get_column_letter(col_num)
        column_dimensions = worksheet.column_dimensions[column_letter]
        column_dimensions.width = column_width

    # Iterate through all movies
    for q in queryset:

        # Define the data for each cell in the row
        row = [
            row_num,
            q.staff_id,
            q.amount,
            q.date_created,
            q.description,
        ]
        row_num += 1

        # Assign the data for each cell of the row
        for col_num, cell_value in enumerate(row, 1):
            cell = worksheet.cell(row=row_num, column=col_num)
            cell.value = str(cell_value)
            cell.alignment = Alignment(wrapText=True)

    workbook.save(response)

    return response


def export_to_xlsx_for_user(queryset, title):
    """
    Exports the queryset to Excel (XLSX) file for a particular user.
    """

    response = HttpResponse(
        content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
    )
    response['Content-Disposition'] = 'attachment; filename={date}-{title}.xlsx'.format(
        date=datetime.now().strftime('%Y-%m-%d'), title=title,
    )
    workbook = Workbook()

    # Get active worksheet/tab
    worksheet = workbook.active
    worksheet.title = title

    # Define the titles for columns
    columns = [
        ('Sr. No.', 10),
        ('Amount', 20),
        ('Date', 20),
        ('Description', 70),
    ]

    row_num = 1
    header_font = Font(bold=True)
    # Assign the titles for each cell of the header
    for col_num, (column_title, column_width) in enumerate(columns, 1):
        cell = worksheet.cell(row=row_num, column=col_num)
        cell.value = column_title
        cell.font = header_font
        cell.alignment = Alignment(horizontal='center')
        # set column width
        column_letter = get_column_letter(col_num)
        column_dimensions = worksheet.column_dimensions[column_letter]
        column_dimensions.width = column_width

    # Iterate through all movies
    for q in queryset:

        # Define the data for each cell in the row
        row = [
            row_num,
            q.amount,
            q.date_created,
            q.description,
        ]
        row_num += 1

        # Assign the data for each cell of the row
        for col_num, cell_value in enumerate(row, 1):
            cell = worksheet.cell(row=row_num, column=col_num)
            cell.value = str(cell_value)
            cell.alignment = Alignment(wrapText=True)

    workbook.save(response)

    return response

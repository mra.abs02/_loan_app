from twilio.rest import Client
from sendgrid import SendGridAPIClient
from sendgrid.helpers.mail import Mail
from loanApp import settings as s
from . import constants as const

client = Client(s.TWILIO_SID, s.TWILIO_TOKEN)


def send_verification_sms(to):
    """
    Sends the twilio verification
    :param to: the user phone number
    :return: the verification sid resource number
    """
    verification = client.verify \
        .services(s.TWILIO_VARIFICATION_SID) \
        .verifications \
        .create(to=to, channel='sms')
    return verification.sid


def check_verification(phone, code):
    """
    check the verification code
    :param phone: the user phone number
    :param code: the verification code entered by the user
    :return: if the verification was successful or not
    """
    try:
        verification_check = client.verify \
            .services(s.TWILIO_VARIFICATION_SID) \
            .verification_checks \
            .create(to=phone, code=code)

        if verification_check.status == "approved":
            return True
        else:
            return False
    except Exception as e:
        print("[Error] Error in verifying the code: ", e)
        return False


def send_email(to, html):
    """
    Send email to the user
    :param to: receiver
    :param html: html file
    :return: whether the email was sent successfully or no
    """
    message = Mail(
        from_email=s.SENDGRID_EMAIL,
        to_emails=to,
        subject=const.EMAIL_SUBJECT,
        html_content=html)
    try:
        sg = SendGridAPIClient(s.SENDGRID_API_KEY)
        response = sg.send(message)
        print("Email receiver: ", to)
        print("status code", response.status_code)
        print("resp body", response.body)
        print("resp header", response.headers)
        print("---------------------")
        return True, response.status_code
    except Exception as e:
        print("[Error]sendgrid error", e.message)
        return False


def send_sms(to, msg):
    """
    send an sms to a user (to).
    """
    message = client.messages \
        .create(body=msg, from_=s.TWILIO_PHONE_NO, to=to)
    print("twilio reply: \n", type(message), message)
    return message.error_message

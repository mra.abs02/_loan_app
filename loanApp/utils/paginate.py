from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from loanApp import settings


def get_contrib_list(staff_obj, page=1, per_page=settings.SHORT_PER_PAGE):
    """
    gets the paginated contribution list
    """
    try:
        contrib_obj = staff_obj.contribution_set.get_queryset().order_by('id')
        contribs_paginator = Paginator(contrib_obj, per_page)
        contribs = contribs_paginator.page(page)
    except PageNotAnInteger:
        contribs = contribs_paginator.page(1)
    except EmptyPage:
        contribs = contribs_paginator.page(contribs_paginator.num_pages)
    return contribs


def get_loan_list(staff_obj, page=1, per_page=settings.SHORT_PER_PAGE):
    """
    Get the loan query list and paginate it
    """
    try:
        loan_obj = staff_obj.loan_set.get_queryset().order_by('id')
        loans_paginator = Paginator(loan_obj, per_page)
        loans = loans_paginator.page(page)
    except PageNotAnInteger:
        loans = loans_paginator.page(1)
    except EmptyPage:
        loans = loans_paginator.page(loans_paginator.num_pages)
    return loans


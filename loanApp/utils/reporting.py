from io import BytesIO
from django.http import HttpResponse
from django.template.loader import get_template

from xhtml2pdf import pisa


def render_template(template_src, context_dict={}, email=False):
    """
    Renders the template
    :param template_src: the template file
    :param context_dict: context dictionary
    :param email: if the template is for the email report
    :return: the rendered html
    """
    if email:
        param = "?u={0}&t={1}&pe={2}&f={3}&l={4}&e={5}&p={6}&c={7}&a={8}&r={9}&m={10}&d={11}&ta={12}".format(
            context_dict['loan']['staff_id'],
            context_dict['loan']['type'],
            context_dict['loan']['period'],
            context_dict['loan']['first_name'],
            context_dict['loan']['last_name'],
            context_dict['loan']['email'],
            context_dict['loan']['phone_no'],
            context_dict['loan']['contract_end'],
            context_dict['loan']['amount'],
            context_dict['loan']['rate'],
            context_dict['loan']['monthly_payment'],
            context_dict['loan']['disbursed_amount'],
            context_dict['loan']["total_amount"],
        )
        context_dict['param'] = param

    template = get_template(template_src)
    return template.render(context_dict)


def render_to_pdf(html_tmp):
    html = html_tmp
    result = BytesIO()
    pdf = pisa.pisaDocument(BytesIO(html.encode("ISO-8859-1")), result)
    if not pdf.err:
        return HttpResponse(result.getvalue(), content_type='application/pdf')
    return None


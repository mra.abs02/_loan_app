import csv


def get_csv():
    with open('loans.csv', mode='r') as csv_file:
        csv_reader = csv.DictReader(csv_file)
        for i in csv_reader:
            print("staff id", i['staff_id'])
            print("type", i['type'])
            print("desc", i['description'])
            print("init amount", i['initial_amount'])
            print("amount", i['amount'])
            print("-------------")


get_csv()
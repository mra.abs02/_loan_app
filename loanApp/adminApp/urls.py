from django.urls import path
from . import views

urlpatterns = [
    path('admin_dashboard', views.admin_dash, name='admin_dash'),
    path('user', views.view_user, name='view_user'),
    path('add_loan', views.add_loan, name='add_loan'),
    path('view_loan', views.view_loan, name='view_loan'),
    path('loan_list', views.view_loan_list, name='loan_list'),
    path('delete_loan/<str:lid>/', views.del_loan, name='del_loan'),
    path('update_loan/<str:sid>/<str:lid>/', views.update_loan, name='update_loan'),
    path('add_contrib', views.add_contrib, name='add_contrib'),
    path('view_contrib', views.view_contrib, name='view_contrib'),
    path('update_contrib/<str:sid>/<str:lid>/', views.update_contrib, name='update_contrib'),
    path('delete_contrib/<str:lid>/', views.del_contrib, name='del_contrib'),
    path('contrib_list', views.view_contrib_list, name='contrib_list'),
    path('add_user', views.add_new_staff, name='add_user'),
    path('del_user/<str:lid>/', views.del_user, name='del_user'),
    path('update_user/<str:sid>/', views.update_staff, name='update_user'),
    path('import_csv', views.import_loan_csv, name='import_csv'),
    path('pending', views.list_pending_loans, name='pending'),
    path('reject_loan', views.reject_loan, name='reject_loan'),
    path('reviewers', views.reviewers, name='reviewers'),
    path('add_reviewer', views.add_reviewer, name='add_reviewer'),
    path('staffs', views.users_list, name='user_list'),
    path('export_loan', views.export_loans, name='export_loan'),
    path('export_contrib', views.export_contrib, name='export_contrib'),
    path('export_user_loan/<str:sid>/', views.export_user_loan, name='export_user_loan'),
    path('export_user_contrib/<str:sid>/', views.export_user_contrib, name='export_user_contrib'),
    path('export_failed', views.export_failed, name='export_failed'),

]

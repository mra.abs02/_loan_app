import threading
from django.shortcuts import render, redirect, reverse
from django.contrib.auth.decorators import user_passes_test
from django.contrib.auth.models import User
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.contrib import messages
from django.db.models import Sum
from loanApp import settings
from .forms import LoanForm, ContribForm, ReviewerForm
from .forms import StaffProfileForm, FileForm
from register.forms import UserForm
from userApp.models import Loan, Contribution
from utils import twilio, constants, reporting, export
from .models import Reviewer

imp_form = FileForm()


@user_passes_test(lambda u: u.is_superuser)
def admin_dash(request):
    user_count = User.objects.all().count()
    loans = Loan.objects.filter(approved=True)
    loan_amount = loans.aggregate(Sum('amount'))
    loan_processing_fee = loans.aggregate(Sum('processing_fee'))
    disbursed_amount = loans.aggregate(Sum('disbursed'))
    print("disbursed: ", loan_processing_fee)
    contribution_amount = Contribution.objects.all().aggregate(Sum('amount'))

    return render(request, 'admin_dash.html', {'user_count': user_count,
                                               'loan_amount': loan_amount,
                                               'disbursed': disbursed_amount,
                                               'processing_fee': loan_processing_fee,
                                               'contrib_amount': contribution_amount, 'imp_form': imp_form})


@user_passes_test(lambda u: u.is_superuser)
def users_list(request):
    user_list = User.objects.get_queryset().order_by('id')
    page = request.GET.get('page', 1)
    paginator = Paginator(user_list, settings.USER_PER_PAGE)
    try:
        users = paginator.page(page)
    except PageNotAnInteger:
        users = paginator.page(1)
    except EmptyPage:
        users = paginator.page(paginator.num_pages)

    return render(request, 'user_list.html', {'users': users, 'imp_form': imp_form})


@user_passes_test(lambda u: u.is_superuser)
def view_user(request):
    pk_id = request.GET.get('id', None)

    context_dict = dict()
    if pk_id is not None:
        try:
            staff = User.objects.get(id=pk_id)
            # print("USER object: ", dir(staff))
            loans = get_loan_list(staff)
            contribs = get_contrib_list(staff)
            context_dict = {'staff': staff, 'loans': loans, 'contribs': contribs, 'imp_form': imp_form}
        except User.DoesNotExist:
            print("[Error] The user id <%s> doesn't exist." % pk_id, len(context_dict))
    # print(context_dict)
    return render(request, 'view_user.html', context_dict)


# --------- CRUD for New Staff -------------
@user_passes_test(lambda u: u.is_superuser)
def add_new_staff(request):
    if request.method == 'POST':
        profile_form = StaffProfileForm(request.POST)
        user_form = UserForm(request.POST)
        if user_form.is_valid() and profile_form.is_valid():
            # #TODO --------- if sms verification is required uncomment and edit the following code:
            # phone_no = profile_form.cleaned_data.get("phone_no")
            # staff_id = user_form.cleaned_data.get("username")
            # print("Your phone number is...", phone_no)
            # vsid = twilio.send_verification_sms(str(phone_no))
            # if vsid is not None:
            #     request.session['phone_no'] = str(phone_no)
            #     request.session['staff_id'] = str(staff_id)
            #     print("vsid: %s for staff_id %s" % (vsid, staff_id))
            #     user_inst = user_form.save()
            #     prof_instance = profile_form.save(commit=False)
            #     prof_instance.user = user_inst
            #     prof_instance.save()
            #     return redirect("/verify")
            user_inst = user_form.save()
            prof_instance = profile_form.save(commit=False)
            prof_instance.user = user_inst
            prof_instance.save()
            messages.success(request, constants.ADD_STAFF_SUCCESS)
            return redirect("/admin_dashboard")
        else:
            print("error user couldn't be created", profile_form.is_valid(), user_form.is_valid())
    else:
        user_form = UserForm()
        profile_form = StaffProfileForm()
    return render(request, 'add_user.html', {'user_form': user_form,
                                             "profile_form": profile_form, 'imp_form': imp_form})


@user_passes_test(lambda u: u.is_superuser)
def del_user(request, lid):
    try:
        user = User.objects.get(id=lid)
    except User.DoesNotExist:
        print("Contribution object doesnt' exist...")
        user = dict()

    if request.method == 'POST':
        user.delete()
        messages.success(request, constants.DEL_STAFF_SUCCESS)
        return redirect("/admin_dashboard")
    return render(request, 'delete_user.html', {'staff': user, 'imp_form': imp_form})


@user_passes_test(lambda u: u.is_superuser)
def update_staff(request, sid):
    try:
        staff = User.objects.get(id=sid)
        user_form = UserForm(instance=staff)
        profile_form = StaffProfileForm(instance=staff.userprofile)
    except User.DoesNotExist:
        staff = dict()
        user_form = dict()
        profile_form = dict()
        print("User doesn't exist")

    if request.method == 'POST':
        profile_form = StaffProfileForm(request.POST, instance=staff.userprofile)
        user_form = UserForm(request.POST, instance=staff)
        if user_form.is_valid() and profile_form.is_valid():
            user_inst = user_form.save()
            prof_instance = profile_form.save(commit=False)
            prof_instance.user = user_inst
            prof_instance.save()
            messages.success(request, constants.UPDATE_STAFF_SUCCESS)
            return redirect("/user?id=%s" % user_inst.id)
        else:
            print("error user couldn't be created", profile_form.is_valid(), user_form.is_valid())

    return render(request, 'update_user.html', {'user_form': user_form,
                                                "profile_form": profile_form, 'imp_form': imp_form})


# --------- CRUD for Contributions ------------
@user_passes_test(lambda u: u.is_superuser)
def add_contrib(request):
    contrib_form = dict()
    pk_id = request.GET.get('id', None)
    staff_name = ''

    if pk_id is None:
        return render(request, 'add_contrib.html', {'form': contrib_form, 'staff': staff_name, 'imp_form': imp_form})

    try:
        staff_obj = User.objects.get(id=pk_id)
        staff_name = staff_obj.username
        contrib_form = ContribForm()

        if request.method == 'POST':
            contrib_form = ContribForm(request.POST)
            # print(loan_form)
            if contrib_form.is_valid():
                contrib_inst = contrib_form.save(commit=False)
                contrib_inst.staff_id = staff_obj
                contrib_inst.save()
                messages.success(request, constants.ADD_CONTRIB_SUCCESS)

                # print("loan added? ", str(added))
                return redirect("/user?id=%s" % staff_obj.id)

    except User.DoesNotExist:
        print("[Error] Could not find the staff.")

    return render(request, 'add_contrib.html', {'form': contrib_form, 'staff': staff_name, 'imp_form': imp_form})


@user_passes_test(lambda u: u.is_superuser)
def view_contrib(request):
    pk_id = request.GET.get('id', None)
    contrib_obj = dict()
    if pk_id is not None:
        try:
            contrib_obj = Contribution.objects.get(id=pk_id)
        except Contribution.DoesNotExist:
            print("[Error] couldn't find the contribution for the user.")
    return render(request, 'contrib_details.html', {"contrib": contrib_obj, 'imp_form': imp_form})


@user_passes_test(lambda u: u.is_superuser)
def update_contrib(request, sid, lid):
    try:
        contrib = Contribution.objects.get(id=lid)
        staff = User.objects.get(id=sid)
        staff_name = staff.username
        contrib_form = ContribForm(instance=contrib)

    except Contribution.DoesNotExist:
        staff_name = ''
        contrib = dict()
        contrib_form = dict()
        print("Contribution doesn't exist")
    except User.DoesNotExist:
        staff_name = ''
        contrib = dict()
        contrib_form = dict()
        print("user doesn't exist")

    if request.method == "POST":
        contrib_form = ContribForm(request.POST, instance=contrib)
        if contrib_form.is_valid():
            contrib_inst = contrib_form.save(commit=False)
            contrib_inst.staff_id = staff
            contrib_inst.save()
            # print("loan added? ", str(added))
            messages.success(request, constants.UPDATE_CONTRIB_SUCCESS)
            return redirect("/view_contrib?id=%s" % contrib_inst.id)

    return render(request, 'update_contrib.html', {'form': contrib_form, 'staff': staff_name, 'imp_form': imp_form})


@user_passes_test(lambda u: u.is_superuser)
def del_contrib(request, lid):
    try:
        contrib = Contribution.objects.get(id=lid)
    except Contribution.DoesNotExist:
        print("Contribution object doesnt' exist...")
        contrib = dict()

    if request.method == 'POST':
        contrib.delete()
        messages.success(request, constants.DEL_CONTRIB_SUCCESS)
        return redirect("/user?id=%s" % contrib.staff_id.id)
    return render(request, 'delete_contrib.html', {'contrib': contrib, 'imp_form': imp_form})


@user_passes_test(lambda u: u.is_superuser)
def view_contrib_list(request):
    pk_id = request.GET.get('id', None)
    page = request.GET.get('page', 1)

    context_dict = dict()
    if pk_id is not None:
        try:
            staff = User.objects.get(id=pk_id)
            print(staff)
            contribs = get_contrib_list(staff, page)
            context_dict = {'contribs': contribs, 'imp_form': imp_form}
        except User.DoesNotExist:
            print("[Error] The user id <%s> doesn't exist." % pk_id, context_dict)
    print(context_dict)
    return render(request, 'contribs_list.html', context_dict)


# -------- CRUD for Loans ------------
@user_passes_test(lambda u: u.is_superuser)
def update_loan(request, sid, lid):
    try:
        loan = Loan.objects.get(id=lid)
        staff = User.objects.get(id=sid)
        staff_name = staff.username
        loan_form = LoanForm(instance=loan)
    except Loan.DoesNotExist:
        staff_name = ''
        loan = dict()
        loan_form = dict()
        print("loan doesn't exist")
    except User.DoesNotExist:
        staff_name = ''
        loan = dict()
        loan_form = dict()
        print("user doesn't exist")

    if request.method == "POST":
        loan_form = LoanForm(request.POST, instance=loan)
        if loan_form.is_valid():
            loan_inst = loan_form.save(commit=False)
            loan_inst.staff_id = staff
            loan_inst.save()
            # print("loan added? ", str(added))
            messages.success(request, constants.UPDATE_LOAN_SUCCESS)
            return redirect("/view_loan?id=%s" % loan_inst.id)

    return render(request, 'update_loan.html', {'form': loan_form, 'staff': staff_name, 'imp_form': imp_form})


@user_passes_test(lambda u: u.is_superuser)
def del_loan(request, lid):
    """
    DELETE the loan recode with loan id = lid
    """
    try:
        loan = Loan.objects.get(id=lid)
    except Loan.DoesNotExist:
        print("loan object doesnt' exist...")
        messages.error(request, constants.INVALID_LOAN)
        loan = dict()

    if request.method == 'POST':
        loan.delete()
        messages.success(request, constants.DEL_LOAN_SUCCESS)
        return redirect("/user?id=%s" % loan.staff_id.id)
    return render(request, 'delete_loan.html', {'loan': loan, 'imp_form': imp_form})


@user_passes_test(lambda u: u.is_superuser)
def add_loan(request):
    staff_name = ''
    if request.method == 'POST':
        pk_id = request.GET.get('id', None)
        staff = User.objects.get(id=pk_id)
        loan_form = LoanForm(request.POST)
        # print(loan_form)
        if loan_form.is_valid():
            loan_inst = loan_form.save(commit=False)
            loan_inst.staff_id = staff
            loan_inst.save()
            # print("loan added? ", str(added))
            messages.success(request, constants.ADD_LOAN_SUCCESS)
            return redirect("/user?id=%s" % staff.id)
        messages.error(request, constants.ADD_LOAN_FAILED)
    else:
        loan_form = dict()
        pk_id = request.GET.get('id', None)

        if pk_id is not None:
            try:
                staff_obj = User.objects.get(id=pk_id)
                staff_name = staff_obj.username
                loan_form = LoanForm()
            except User.DoesNotExist:
                messages.error(request, constants.INVALID_STAFF)
                print("[Error] Could not find the staff.")

    return render(request, 'add_loan.html', {'form': loan_form, 'staff': staff_name, 'imp_form': imp_form})


@user_passes_test(lambda u: u.is_superuser)
def view_loan_list(request):
    pk_id = request.GET.get('id', None)
    page = request.GET.get('page', 1)

    context_dict = dict()
    if pk_id is not None:
        try:
            staff = User.objects.get(id=pk_id)
            print(staff)
            loans = get_loan_list(staff, page, settings.TRANS_PER_PAGE)
            context_dict = {'loans': loans, 'imp_form': imp_form}
        except User.DoesNotExist:
            print("[Error] The user id <%s> doesn't exist." % pk_id, len(context_dict))
    print(context_dict)
    return render(request, 'loans_list.html', context_dict)


@user_passes_test(lambda u: u.is_superuser)
def view_loan(request):
    pk_id = request.GET.get('id', None)
    loan_obj = dict()
    if pk_id is not None:
        try:
            loan_obj = Loan.objects.get(id=pk_id)
        except Loan.DoesNotExist:
            print("[Error] couldn't find the loan for the user.")
    return render(request, 'loan_details.html', {"loan": loan_obj, 'imp_form': imp_form})


@user_passes_test(lambda u: u.is_superuser)
def reject_loan(request):
    if request.method == 'POST':
        lid = request.POST.get('reject', None)
        desc = request.POST.get("desc", None)
        if desc is not None and lid is not None:
            # save into the db
            loan_obj = Loan.objects.get(id=lid)
            loan_obj.approved = False
            loan_obj.description = desc
            loan_obj.save()

            # send sms and email
            user_dict = dict()
            user_dict['obj'] = loan_obj.staff_id
            user_dict['status'] = 'false'
            user_dict['desc'] = desc
            # communicate_request_status([user_dict], "loan")
            threading.Thread(target=communicate_request_status, args=([user_dict], "loan",)) \
                .start()
            # print("Loan is accepted for %s: ", loan, type(loan))
            return redirect("/pending")


@user_passes_test(lambda u: u.is_superuser)
def list_pending_loans(request):
    """
    list the pending loans. Also, allows approval of the loans.
    """
    if request.method == 'POST':
        desc = request.POST.get("desc", None)
        lid = request.POST.get("approve", None)
        if desc is not None and lid is not None:
            loan_obj = Loan.objects.get(id=lid)
            loan_obj.processing_fee = loan_obj.initial_amount * settings.DEFAULT_PROCESSING_FEE / 100
            loan_obj.approved = True
            loan_obj.description = desc
            loan_obj.save()
            user_dict = dict()
            user_dict['obj'] = loan_obj.staff_id
            user_dict['status'] = 'true'
            user_dict['desc'] = desc
            # communicate_request_status([user_dict], "loan")
            # TODO to spawn a new thread for sending email if required
            threading.Thread(target=communicate_request_status, args=([user_dict], "loan",)) \
                .start()
            return redirect("/pending")

    loan_list = Loan.objects.filter(approved=None).order_by('id')
    # user_list = User.objects.get_queryset().order_by('id')
    page = request.GET.get('page', 1)
    paginator = Paginator(loan_list, settings.USER_PER_PAGE)
    try:
        loans = paginator.page(page)
    except PageNotAnInteger:
        loans = paginator.page(1)
    except EmptyPage:
        loans = paginator.page(paginator.num_pages)

    return render(request, 'pending_loans.html', {'loans': loans, 'imp_form': imp_form})


@user_passes_test(lambda u: u.is_superuser)
def import_loan_csv(request):
    import csv
    if request.method == 'POST':
        file_form = FileForm(request.POST, request.FILES)
        total_record = 0
        valid_users = []
        users_with_multiple_application = []
        users_with_no_application = []
        invalid_users = set()
        if file_form.is_valid():
            import_type = request.POST.get('import-type')
            if import_type == 'loans':
                app_type = 'loan'
            else:
                app_type = 'contribution'

            # print("import type: ", form.cleaned_data['file'], import_type)
            # with open(file_form.cleaned_data['file'].read().decode(), mode='r') as csv_file:
            #     csv_file = file_form.cleaned_data['file'].read().decode()

            decoded_file = file_form.cleaned_data['file'].read().decode('utf-8').splitlines()
            # print("Decoded file ", decoded_file)
            csv_reader = csv.DictReader(decoded_file)

            for ln in csv_reader:
                # print("The record: ", ln)
                total_record += 1
                try:
                    staff = User.objects.get(username=ln['id'])
                    # print("import: ", staff, ln['description'])
                    user_dict = dict()
                    user_dict['obj'] = staff
                    user_dict['status'] = ln['approved']
                    user_dict['desc'] = ln['description']
                except User.DoesNotExist:
                    print("[Error] Failed to import. User doesn't exist: ", ln['id'])
                    invalid_users.add(ln['id'])
                    continue
                # TODO to change the following if required
                if ln['approved'].lower() == 'true' or ln['approved'].lower() == 'yes':
                    approved = True
                else:
                    approved = False

                if import_type == 'loans':
                    try:
                        loan = Loan.objects.get(staff_id=staff, approved=None)
                        loan.description = ln['description']
                        loan.approved = approved
                        loan.amount = ln['amount']
                        if approved:
                            loan.processing_fee = loan.initial_amount * settings.DEFAULT_PROCESSING_FEE / 100
                        loan.save()
                    except Loan.MultipleObjectsReturned:
                        print("[Error] %s has multiple loan application." % staff.username)
                        users_with_multiple_application.append(staff.username)
                        continue
                    except Loan.DoesNotExist:
                        print("[Error] %s has no loan application." % staff.username)
                        users_with_no_application.append(staff.username)
                        continue
                    # loan = Loan.objects.update_or_create(
                    #     staff_id=staff,
                    #     approved=None,
                    #     defaults={'description': ln['description'],
                    #               'approved': approved,
                    #               'amount': ln['amount']},
                    # )
                    # print(loan)
                else:
                    contrib = Contribution.objects.create(
                        staff_id=staff,
                        description=ln['description'],
                        amount=ln['amount'],
                    )
                    # print(contrib)
                valid_users.append(user_dict)
            print("list of valid users %s", valid_users)

            # communicate_request_status(valid_users, app_type)
            # spawn send sms/email thread
            threading.Thread(target=communicate_request_status, args=(valid_users, app_type,)) \
                .start()

        print("Number of error while inserting the loans/contrib %s out of %s. Invalid users: %s" %
              (total_record - len(valid_users), total_record, invalid_users))
        if total_record - len(valid_users) == 0:
            messages.success(request, constants.IMPORT_SUCCESS)
        else:
            messages.error(request, constants.IMPORT_FAILED % (len(valid_users), invalid_users))

        print("The following has mult application", users_with_multiple_application,
              len(users_with_multiple_application) > 1)
        if len(users_with_multiple_application) > 0:
            messages.error(request, constants.IMPORT_FAILED_MULTIPLE_APPLICATIONS % users_with_multiple_application)

        print("The following has nos application", users_with_no_application)
        if len(users_with_no_application) > 0:
            messages.error(request, constants.IMPORT_FAILED_NO_APPLICATION % users_with_no_application)

    return redirect("/admin_dashboard")


@user_passes_test(lambda u: u.is_superuser)
def reviewers(request):
    reviewers_list = Reviewer.objects.all()
    return render(request, 'reviewers.html', {'reviewers': reviewers_list, 'imp_form': imp_form})


@user_passes_test(lambda u: u.is_superuser)
def add_reviewer(request):
    reviewer_form = ReviewerForm()

    if request.method == 'POST':
        reviewer_form = ReviewerForm(request.POST)
        if reviewer_form.is_valid():
            reviewer_form.save()
            return redirect('/reviewers')

    return render(request, 'add_reviewer.html', {'add_reviewer_form': reviewer_form, 'imp_form': imp_form})


@user_passes_test(lambda u: u.is_superuser)
def export_loans(request):
    if 'download' in request.GET:
        loans = Loan.objects.all()
        file_request = export.export_to_xlsx(loans, constants.EXPORT_ALL_LOANS_TITLE)
        return file_request
    else:
        return render(request, 'download.html', {'imp_form': imp_form})


@user_passes_test(lambda u: u.is_superuser)
def export_contrib(request):
    if 'download' in request.GET:
        loans = Contribution.objects.all()
        file_request = export.export_to_xlsx(loans, constants.EXPORT_ALL_LOANS_TITLE)
        return file_request
    else:
        return render(request, 'download.html', {'imp_form': imp_form})


@user_passes_test(lambda u: u.is_superuser)
def export_user_loan(request, sid):
    if 'download' in request.GET:
        try:
            staff = User.objects.get(id=sid)
        except User.DoesNotExist:
            print("faled")
            return redirect(reverse('export_failed'))

        loans = staff.loan_set.all()
        file_request = export.export_to_xlsx_for_user(loans, constants.EXPORT_LOAN_FOR_USER % staff.first_name)
        return file_request
    else:
        return render(request, 'download.html', {'imp_form': imp_form})


@user_passes_test(lambda u: u.is_superuser)
def export_user_contrib(request, sid):
    if 'download' in request.GET:
        try:
            staff = User.objects.get(id=sid)
        except User.DoesNotExist:
            print("faled")
            return redirect(reverse('export_failed'))

        loans = staff.contribution_set.all()
        file_request = export.export_to_xlsx_for_user(loans, constants.EXPORT_CONTIRB_FOR_USER % staff.first_name)
        return file_request
    else:
        return render(request, 'download.html', {'imp_form': imp_form})


@user_passes_test(lambda u: u.is_superuser)
def export_failed(request):
    print("loaded")
    return render(request, 'download_failed.html', {'imp_form': imp_form})


def communicate_request_status(user_list, application_type):
    """
    It sends email and sms to the users, regarding their loan or contribution status.
    """
    for u in user_list:

        if u['status'].lower() == 'true' or u['status'].lower() == 'yes':
            approved = 'approved'
        else:
            approved = 'rejected'

        # *** send sms
        sms_msg = constants.SMS_MSG % (u['obj'].first_name, application_type, approved, u['desc'].capitalize())
        print("communicate_request_status: ", sms_msg)
        sms = twilio.send_sms(str(u['obj'].userprofile.phone_no), sms_msg)  # get the error if it is not None
        print("Error for twilio-sms for staff %s : %s" % (u['obj'].first_name, sms))

        # *** send email
        context_dict = {'type': application_type,
                        'name': u['obj'].first_name + ' ' + u['obj'].last_name,
                        'status': approved,
                        'desc': u['desc']}

        email_html = reporting.render_template(template_src='status_report.html', context_dict=context_dict)
        print(">> EMAIL Report: \n", email_html)

        # check if status is not 200, 202, 2XX, to be implemented if required
        email_succ, em_status = twilio.send_email(u['obj'].email, email_html)
        print("Email success? ", email_succ)


def get_loan_list(staff_obj, page=1, per_page=settings.SHORT_PER_PAGE):
    try:
        loan_obj = staff_obj.loan_set.get_queryset().order_by('id')
        loans_paginator = Paginator(loan_obj, per_page)
        loans = loans_paginator.page(page)
    except PageNotAnInteger:
        loans = loans_paginator.page(1)
    except EmptyPage:
        loans = loans_paginator.page(loans_paginator.num_pages)
    return loans


def get_contrib_list(staff_obj, page=1, per_page=settings.SHORT_PER_PAGE):
    """
    gets the paginated contribution list
    """
    try:
        contrib_obj = staff_obj.contribution_set.get_queryset().order_by('id')
        contribs_paginator = Paginator(contrib_obj, per_page)
        contribs = contribs_paginator.page(page)
    except PageNotAnInteger:
        contribs = contribs_paginator.page(1)
    except EmptyPage:
        contribs = contribs_paginator.page(contribs_paginator.num_pages)
    return contribs

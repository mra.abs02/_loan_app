from django.db import models
from phonenumber_field.modelfields import PhoneNumberField


class Reviewer(models.Model):
    name = models.CharField(max_length=200, null=True)
    email = models.EmailField()
    phone_no = PhoneNumberField(blank=True, null=True)

    def __str__(self):
        return self.name

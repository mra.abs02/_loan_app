from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from phonenumber_field.formfields import PhoneNumberField
from userApp.models import Loan as LoanModel
from userApp.models import Contribution as ContribModel
from register.models import UserProfile
from .models import Reviewer


class ReviewerForm(forms.ModelForm):
    """
    The form for the Reviewers model
    """
    class Meta:
        model = Reviewer
        fields = '__all__'


class FileForm(forms.Form):
    file = forms.FileField(label="Select a csv file to import", widget=forms.FileInput(attrs={'accept': '.csv'}))


class LoanForm(forms.ModelForm):
    LOAN_CHOICES = (
        ('NEW', 'NEW'),
        ('Top Up', 'Top Up'),
        ('Emergency', 'Emergency')
    )
    status = forms.CharField(label="Loan Type", widget=forms.Select(choices=LOAN_CHOICES), required=False)
    description = forms.CharField(required=False)

    class Meta:
        model = LoanModel
        fields = ('status', 'description', 'interest', 'amount', 'approved')


class ContribForm(forms.ModelForm):
    class Meta:
        model = ContribModel
        fields = ('amount', 'description')


class StaffProfileForm(forms.ModelForm):
    phone_no = PhoneNumberField(required=True)
    net_sal = forms.FloatField(required=True, label="Net Salary")
    verified = forms.BooleanField(required=False, label="Verified Phone No.")
    contract_end = forms.DateField(required=True, label="Contract End Date",
                                   widget=forms.DateInput(attrs={'class': 'form-control', 'placeholder': "Select date",
                                                                 'type': 'date'}))

    class Meta:
        model = UserProfile
        fields = ["phone_no", "location", "contract_end", "net_sal", "verified"]

function mortgage(amount, period, interest) {
    var m = amount * (interest * (1 + interest) ** period) / ((1 + interest) ** period - 1);
    return m;
}
"""
Django settings for loanApp project.

Generated by 'django-admin startproject' using Django 3.1.1.

For more information on this file, see
https://docs.djangoproject.com/en/3.1/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/3.1/ref/settings/
"""

from pathlib import Path
import os
import pymysql

pymysql.version_info = (1, 4, 6, 'final', 0)
pymysql.install_as_MySQLdb()

# Build paths inside the project like this: BASE_DIR / 'subdir'.
BASE_DIR = Path(__file__).resolve().parent.parent.parent
print("base path", BASE_DIR)
ALLOWED_HOSTS = ['*']
# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/3.1/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '6icey@e3#*xqcugs(^ncmyu18t8-2o)t!oz$27ka*@r1k(pbr*'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

# Application definition

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'userApp',
    'register',
    'adminApp',
    'phonenumber_field',
    'crispy_forms',
    'django.contrib.humanize',
    'django.contrib.auth',
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'loanApp.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'loanApp.wsgi.application'

# ---- Database Config
# https://docs.djangoproject.com/en/3.1/ref/settings/#databases
## Database to Gcloud and local
# ****[START db_setup]
# if os.getenv('GAE_APPLICATION', None):
#     # Running on production App Engine, so connect to Google Cloud SQL using
#     # the unix socket at /cloudsql/<your-cloudsql-connection string>
#     DATABASES = {
#         'default': {
#             'ENGINE': 'django.db.backends.mysql',
#             'HOST': '/cloudsql/possible-cycle-288014:us-central1:loan-instance',
#             'USER': 'loan_db',
#             'PASSWORD': 'pass123',
#             'NAME': 'loan_db',
#         }
#     }
# else:
#     DATABASES = {
#         'default': {
#             'ENGINE': 'django.db.backends.mysql',
#             'HOST': '127.0.0.1',
#             'PORT': '3306',
#             'USER': 'loan_db',
#             'PASSWORD': 'pass123',
#             'NAME': 'loan_db',
#         }
#     }
# ***** sqlite ****
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': BASE_DIR / 'db.sqlite3',
    }
}

# ----- End Database config

# Password validation
# https://docs.djangoproject.com/en/3.1/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

# Internationalization
# https://docs.djangoproject.com/en/3.1/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/3.1/howto/static-files/

STATIC_URL = '/static/'
STATICFILES_DIRS = (
    BASE_DIR / 'static/',
)
# STATIC_ROOT = 'static'

# crispy form template
CRISPY_TEMPLATE_PACK = "bootstrap4"

# #TWILIO Maurice
# # TODO to place the keys in us env
# TWILIO_PHONE_NO = '+12015975519'
# TWILIO_SID = 'AC8739824d3dac6d4b1b650043f2abe9f6'
# TWILIO_TOKEN = '5ea0eb4dc8e0e5e61c488e90da4ee036'
# TWILIO_VARIFICATION_SID = 'VA345ccbba75e6c0724056ccd4e6304ce1'
# SENDGRID_API_KEY = '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'
# SENDGRID_EMAIL = 'mra.abs02@gmail.com'

# TWILIO Developer
TWILIO_PHONE_NO = '+12135315940'
TWILIO_SID = 'ACfec7b7480ff1d149e8e687fbea04748a'
TWILIO_TOKEN = '65e3baf2c3bc35b278019c21a969697c'
TWILIO_VARIFICATION_SID = 'VAa2ec0014c1e90c23d306adc84a34fa68'
SENDGRID_API_KEY = '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'
SENDGRID_EMAIL = 'mra.abs02@gmail.com'

# login/out redirects
LOGIN_URL = '/login'
LOGIN_REDIRECT_URL = "/check"
LOGOUT_REDIRECT_URL = "/"

# number of users to show per page in the admin dashboard
USER_PER_PAGE = 10
SHORT_PER_PAGE = 10
TRANS_PER_PAGE = 10

# Default interest rate
DEFAULT_INTEREST_RATE = 2
DEFAULT_DISBURSED = 0.985
DEFAULT_PROCESSING_FEE = 1.5

# EMAIL BACKEND FOR PASSWORD RECOVERY (ONLY DEVELOPMENT)
EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

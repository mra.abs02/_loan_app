from django.urls import path
from . import views

urlpatterns = [
    path('', views.main, name='home'),
    path('dashboard', views.dashboard, name='dashboard'),
    path('loan', views.loan_trans, name='loan_trans'),
    path('contrib', views.contrib_trans, name='contrib_trans'),
    path('application', views.loan_application, name='loan_application'),
    path('dw_report', views.generate_pdf_for_review, name='dw_report'),
    path('about_us', views.about_us, name='about_us'),
    path('contact_us', views.contact_us, name='contact_us'),
    path('download_contrib', views.download_contrib, name='download_contrib'),
    path('download_loan', views.download_loan, name='download_loan'),
]

from django import forms
from .models import Loan
from loanApp.settings import DEFAULT_INTEREST_RATE


class LoanApplication(forms.ModelForm):
    LOAN_CHOICES = (
        ('NEW', 'NEW'),
        ('Top Up', 'Top Up'),
        ('Emergency', 'Emergency')
    )
    status = forms.CharField(label="Loan Type", widget=forms.Select(choices=LOAN_CHOICES))
    period = forms.IntegerField(required=True, label="Period in Months")
    initial_amount = forms.FloatField(required=True)
    monthly_payment = forms.CharField(label="Monthly Payment Amount", required=True,
                              widget=forms.NumberInput(attrs={'readonly': True}))

    disbursed_amount = forms.FloatField(label="Disbursed Amount", required=True,
                                   widget=forms.NumberInput(attrs={'readonly': True}))

    amount = forms.FloatField(label="Total Amount", required=True,
                              widget=forms.NumberInput(attrs={'readonly': True}))

    interest = forms.FloatField(required=True, label="Interest Rate",
                                widget=forms.NumberInput(attrs={
                                    'value': DEFAULT_INTEREST_RATE,
                                    'readonly': True}))

    class Meta:
        model = Loan
        fields = ("status", 'initial_amount', "period", 'interest', "amount")

from django.db import models
from django.contrib.auth.models import User
from loanApp.settings import DEFAULT_INTEREST_RATE


class Contribution(models.Model):
    staff_id = models.ForeignKey(User, null=True, on_delete=models.SET_NULL)
    date_created = models.DateField(auto_now_add=True, null=True)
    description = models.CharField(max_length=200)
    amount = models.FloatField(null=True)

    def __str__(self):
        return self.amount


class Loan(models.Model):
    STATUS = (
        ('NEW', 'NEW'),
        ('Top Up', 'Top Up'),
        ('Emergency', 'Emergency')
    )
    staff_id = models.ForeignKey(User, null=True, on_delete=models.SET_NULL)
    status = models.CharField(max_length=100, null=True, choices=STATUS)
    date_created = models.DateField(auto_now_add=True, null=True)
    description = models.CharField(max_length=200, null=True)
    initial_amount = models.FloatField(null=True)
    interest = models.FloatField(default=DEFAULT_INTEREST_RATE, null=True)
    amount = models.FloatField(null=True)
    processing_fee = models.FloatField(null=True)
    disbursed = models.FloatField(null=True)
    approved = models.BooleanField(null=True)

    def save(self, *args, **kwargs):
        if not self.approved:
            self.processing_fee = None
        super().save(*args, **kwargs)

    def __str__(self):
        return self.amount

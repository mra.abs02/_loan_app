import threading
from django.http import HttpResponse
from django.shortcuts import render, redirect, reverse
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.utils.safestring import mark_safe
from utils import reporting, twilio, constants
from utils import paginate, export
from .forms import LoanApplication
from .models import Loan
from register.models import User
from loanApp import settings
from django.db.models import Sum
from adminApp.models import Reviewer

loan_form = LoanApplication()


def main(request):
    # print("USER OBJECT TYPE: ", type(request.user))
    return render(request, 'home.html', {"form": loan_form, 'disbursed': settings.DEFAULT_DISBURSED})


def check_user(request):
    print("check user: ", request.user.is_superuser)
    if request.user.is_superuser:
        return redirect("/admin_dashboard")
    return redirect("/dashboard")


@login_required
def dashboard(request):
    total_loans = 0.0
    total_contribs = 0.0
    if not request.user.is_superuser:
        total_loans = request.user.loan_set.filter(approved=True).aggregate(Sum('amount'))
        total_contribs = request.user.contribution_set.all().aggregate(Sum('amount'))
        if not request.user.userprofile.verified:
            request.session['phone_no'] = str(request.user.userprofile.phone_no)
            request.session['staff_id'] = str(request.user.username)
            print("user is not valid staff_id ", request.user.username)
            redirect('/logout')
            return render(request, 'not_verified.html', {})

    return render(request, 'dashboard.html', {"form": loan_form,
                                              'disbursed': settings.DEFAULT_DISBURSED,
                                              'total_loans': total_loans,
                                              'total_contribs': total_contribs})


@login_required
def contrib_trans(request):
    page = request.GET.get('page', 1)
    contribs = paginate.get_contrib_list(request.user, page, settings.TRANS_PER_PAGE)
    return render(request, 'contrib_trans.html', {"form": loan_form,
                                                  'contribs': contribs,
                                                  'disbursed': settings.DEFAULT_DISBURSED})


@login_required
def loan_trans(request):
    page = request.GET.get('page', 1)
    loans = paginate.get_loan_list(request.user, page, settings.TRANS_PER_PAGE)
    return render(request, 'loan_trans.html', {"form": loan_form,
                                               'loans': loans,
                                               'disbursed': settings.DEFAULT_DISBURSED})


@login_required
def loan_application(request):
    if request.method == "POST":
        # print("loan application is called...")
        loan_app = LoanApplication(request.POST)
        # print("Loan App", loan_app, "\n")
        # print("Loan POST", request.POST, "\n")
        if loan_app.is_valid():
            # print("USER OBJECT TYPE: ", type(request.user))

            # save the loan in the database
            # loan_app.save(commit=False)
            print("disbursed : ", loan_app.cleaned_data['disbursed_amount'])
            print("disbursed2 : ", request.POST.get("disbursed_amount"))

            loan_obj = request.user.loan_set.filter(approved=None)
            if loan_obj.count() > 0:
                error_msg = constants.LOAN_APPLICATION_ERROR % request.user.first_name
                print("More than 1 loan", error_msg)
                messages.error(request, error_msg)
                return redirect(reverse('dashboard'))

            loan_model = Loan.objects.create(
                staff_id=request.user,
                amount=-loan_app.cleaned_data['amount'],
                initial_amount=loan_app.cleaned_data.get('initial_amount'),
                approved=None,
                disbursed=loan_app.cleaned_data['disbursed_amount']
            )
            print(loan_model.staff_id)

            # create the loan pdf and send the email
            loan = dict()
            loan['staff_id'] = request.user.username
            loan['type'] = str(loan_app.cleaned_data.get('status'))
            loan['period'] = str(loan_app.cleaned_data.get('period'))
            loan['first_name'] = request.user.first_name
            loan['last_name'] = request.user.last_name
            loan['email'] = request.user.email
            loan['phone_no'] = request.user.userprofile.phone_no
            loan['contract_end'] = request.user.userprofile.contract_end
            loan['amount'] = str(loan_app.cleaned_data.get('initial_amount'))
            loan['rate'] = request.POST.get("interest")
            loan['monthly_payment'] = request.POST.get("monthly_payment")
            loan['disbursed_amount'] = request.POST.get("disbursed_amount")
            loan["total_amount"] = request.POST.get("amount")

            # generate pdf report and email to the reviewers
            context_dict = {"loan": loan,
                            "host": request.scheme + "://" + request.META['HTTP_HOST']}

            href = generate_href(context_dict)
            success_message = constants.LOAN_APPLICATION_SUCCESS % href
            # print("loan application ", success_message)
            messages.success(request, mark_safe(success_message))
            # TODO disable multithreading if the number of user is large
            threading.Thread(target=communicate_with_reviewer, args=(context_dict,)).start()
            #     communicate_with_reviewer(context_dict)

            # html = reporting.render_template('loan_application_report.html', context_dict)
            # pdf = reporting.render_to_pdf(html)
            # if pdf is not None:
            #     # if not email_succ:
            #     #     print(constants.EMAIL_TO_ADMIN_FAILED)
            #     return HttpResponse(pdf, content_type='application/pdf')
            # else:
            #     messages.error(request, constants.LOAN_APPLICATION_ERROR)
    return redirect('/dashboard')


@login_required
def download_contrib(request):
    if 'download' in request.GET:

        loans = request.user.contribution_set.all()
        file_request = export.export_to_xlsx_for_user(
            loans, constants.EXPORT_CONTIRB_FOR_USER % request.user.first_name
        )
        return file_request
    else:
        return render(request, 'download.html', {"form": loan_form, 'disbursed': settings.DEFAULT_DISBURSED})


@login_required
def download_loan(request):
    if 'download' in request.GET:

        loans = request.user.loan_set.all()
        file_request = export.export_to_xlsx_for_user(
            loans, constants.EXPORT_LOAN_FOR_USER % request.user.first_name
        )
        return file_request
    else:
        return render(request, 'download.html', {"form": loan_form, 'disbursed': settings.DEFAULT_DISBURSED})


def generate_pdf_for_review(request):
    loan = dict()
    loan['staff_id'] = request.GET.get('u')
    loan['type'] = request.GET.get('t')
    loan['period'] = request.GET.get('pe')
    loan['first_name'] = request.GET.get('f')
    loan['last_name'] = request.GET.get('l')
    loan['email'] = request.GET.get('e')
    loan['phone_no'] = request.GET.get('p')
    loan['contract_end'] = request.GET.get('c')
    loan['amount'] = request.GET.get('a')
    loan['rate'] = request.GET.get('r')
    loan['monthly_payment'] = request.GET.get('m')
    loan['disbursed_amount'] = request.GET.get('d')
    loan["total_amount"] = request.GET.get('ta')
    context_dict = {"loan": loan}
    html = reporting.render_template('loan_application_report.html', context_dict)
    pdf = reporting.render_to_pdf(html)
    if pdf is not None:
        return HttpResponse(pdf, content_type='application/pdf')
    else:
        return HttpResponse(constants.REVIEWER_LOAN_APPLICATION_ERROR)


def about_us(request):
    return render(request, 'about_us.html', {"form": loan_form, 'disbursed': settings.DEFAULT_DISBURSED})


def contact_us(request):
    return render(request, 'contact_us.html', {"form": loan_form, 'disbursed': settings.DEFAULT_DISBURSED})


def communicate_with_reviewer(context_dict):
    """
    It sends email to all the reviewers, regarding the new loans.
    """
    email_html = reporting.render_template('email_report.html',
                                           context_dict, True)
    print(">> EMAIL Report: ", email_html)

    reviewers_list = Reviewer.objects.all()
    for r in reviewers_list:
        email_succ = twilio.send_email(r.email, email_html)
        print("sending the email to the reviewer %s was successful: %s" % (r.name, email_succ))
        # TODO to handle the twilio errors if required


def generate_href(context_dict):
    param = "?u={0}&t={1}&pe={2}&f={3}&l={4}&e={5}&p={6}&c={7}&a={8}&r={9}&m={10}&d={11}&ta={12}".format(
        context_dict['loan']['staff_id'],
        context_dict['loan']['type'],
        context_dict['loan']['period'],
        context_dict['loan']['first_name'],
        context_dict['loan']['last_name'],
        context_dict['loan']['email'],
        context_dict['loan']['phone_no'],
        context_dict['loan']['contract_end'],
        context_dict['loan']['amount'],
        context_dict['loan']['rate'],
        context_dict['loan']['monthly_payment'],
        context_dict['loan']['disbursed_amount'],
        context_dict['loan']["total_amount"],
    )
    # context_dict['param'] = param
    href = "{0}{1}{2}".format(context_dict['host'], reverse('dw_report'), param)
    return href

from django.contrib.auth.forms import UserCreationForm
from django import forms
from django.contrib.auth.models import User
from phonenumber_field.formfields import PhoneNumberField
from .models import UserProfile
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Field, Div


class UserForm(UserCreationForm):
    username = forms.CharField(required=True, max_length=6, label='Staff ID')
    email = forms.EmailField(required=True)
    first_name = forms.CharField(required=True)
    last_name = forms.CharField(required=True)

    class Meta:
        model = User
        fields = ["username", "first_name", "last_name", "email", "password1", "password2"]


class ProfileForm(forms.ModelForm):
    phone_no = PhoneNumberField(required=True, help_text="Enter your mobile number with country code.")
    net_sal = forms.FloatField(required=True, label="Net Salary")
    contract_end = forms.DateField(required=True, label="Contract End Date",
                                   widget=forms.DateInput(attrs={'class': 'form-control', 'placeholder': "Select date",
                                                                 'type': 'date'}))

    class Meta:
        model = UserProfile
        fields = ["phone_no", "net_sal", "location", "contract_end"]



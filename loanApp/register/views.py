from django.shortcuts import render, redirect
from django.contrib import messages
from . import models
from . import forms
from utils import twilio, constants


def register(request):
    if request.method == 'POST':
        profile_form = forms.ProfileForm(request.POST)
        user_form = forms.UserForm(request.POST)
        if user_form.is_valid() and profile_form.is_valid():
            phone_no = profile_form.cleaned_data.get("phone_no")
            staff_id = user_form.cleaned_data.get("username")
            print("Your phone number is...", phone_no)
            vsid = twilio.send_verification_sms(str(phone_no))
            if vsid is not None:
                request.session['phone_no'] = str(phone_no)
                request.session['staff_id'] = str(staff_id)
                print("vsid: %s for staff_id %s" % (vsid, staff_id))
                user_inst = user_form.save()
                prof_instance = profile_form.save(commit=False)
                prof_instance.user = user_inst
                prof_instance.save()
                return redirect("/verify")
        else:
            print("error user couldn't be created", profile_form.is_valid(), user_form.is_valid())
    else:
        user_form = forms.UserForm()
        profile_form = forms.ProfileForm()
    return render(request, 'register.html', {'user_form': user_form,
                                             "profile_form": profile_form})


def verify(request):
    """
    The view for verification of the phone number
    :param request: html request
    :return: renders the verify html
    """
    if request.method == "POST":
        phone_no = request.session.get("phone_no", None)
        staff_id = request.session.get("staff_id", None)
        print("phone_no %s staff_id %s" %(phone_no, staff_id))
        if phone_no is not None and staff_id is not None:
            print("verify function: phone no:  ", phone_no)
            valid = twilio.check_verification(phone_no, request.POST.get("code"))
            if valid:
                print("phone number is verified")
                staff = models.UserProfile.objects.filter(user__username=staff_id).update(verified=True)
                messages.success(request, constants.ACCOUNT_CREATED)
                return redirect("/login")
            else:
                print("Phone number is not valid")
    return render(request, "verify.html", {})


def resend_sms_verification(request):
    phone_no = request.session.get("phone_no", None)
    staff_id = request.session.get("staff_id", None)
    print(">> phone_no %s staff_id %s" % (phone_no, staff_id))
    vsid = twilio.send_verification_sms(str(phone_no))
    print("resend sms: ", vsid)
    return redirect('/verify')

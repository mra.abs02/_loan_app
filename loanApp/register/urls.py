from django.urls import path
from . import views

urlpatterns = [
    path('register/', views.register, name='signup'),
    path('verify', views.verify, name='phone_verify'),
    path('resend_sms', views.resend_sms_verification, name='resend_sms')
]

from django.db import models
from django.contrib.auth.models import User
from phonenumber_field.modelfields import PhoneNumberField


class UserProfile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    phone_no = PhoneNumberField(blank=True, null=True)
    location = models.CharField(max_length=20, null=True)
    contract_end = models.DateField(null=True)
    net_sal = models.FloatField(null=True)
    verified = models.BooleanField(default=False)
